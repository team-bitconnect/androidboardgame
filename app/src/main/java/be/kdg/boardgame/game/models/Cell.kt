package be.kdg.boardgame.game.models

data class Cell(var x: Int, var y: Int, var multiplier: Int, var score: Int, var char: Char) {

}