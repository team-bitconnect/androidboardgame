package be.kdg.boardgame.game.dto

data class WordScore (
    val word: String = "",
    val score: Int = 0
)