package be.kdg.boardgame.game.models

import be.kdg.boardgame.R

data class TileBinding(
    var char: Char = '<',
    var pos: Int = -1,
    var isRackTile: Boolean = false,
    var isEmpty: Boolean = false,
    val drawable: Int? = R.drawable.a_en_us
) {

}