package be.kdg.boardgame.game;

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.*
import be.kdg.boardgame.BaseNavigationActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.authentication.services.AuthService
import be.kdg.boardgame.game.dto.*
import be.kdg.boardgame.game.services.GameService
import be.kdg.boardgame.game.services.LetterRackService
import be.kdg.boardgame.game.models.TileBinding
import be.kdg.boardgame.gamecreation.dto.Game
import be.kdg.boardgame.gamecreation.dto.Score
import be.kdg.boardgame.gamecreation.dto.User
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

/**
 * @author Quinten Spillemaeckers
 * this activity will programmatically create different layouts and views
 * and will make ImageViews (placeholder for tile images) clickable
 * to enable the placement of tiles*/
class GameActivity : BaseNavigationActivity() {
    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private lateinit var letterRackService: LetterRackService
    private lateinit var authService: AuthService
    private lateinit var gameService: GameService
    private var accessToken: String? = null
    private var myUsername: String? = null

    private lateinit var imageViewsBoard: Array<ImageView>
    private lateinit var gridBoard: GridLayout
    private lateinit var linearLayouts: Array<LinearLayout>
    private lateinit var savedImageView: ImageView
    private lateinit var savedPlayerTiles: MutableList<TileBinding>

    private lateinit var savedImageViewsRack: MutableList<ImageView>
    private lateinit var savedLinearLayout: LinearLayout

    private lateinit var gridRack: GridLayout
    private lateinit var imageViewsRack: Array<ImageView>
    private lateinit var linearLayoutsRack: Array<LinearLayout>

    private lateinit var frameLayoutBoard: FrameLayout
    private lateinit var boardImageView: ImageView

    private lateinit var myIntent: Intent
    private lateinit var bundle: Bundle
    private var currentlyPlaying: String = ""
    private var isMyTurn: Boolean = false
    private var hasStarted: Boolean = true

    private var lettersOnRack: MutableList<TileBinding> = mutableListOf()

    private lateinit var layButton: Button
    private lateinit var backButton: Button
    private lateinit var chatButton: Button

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()

        setContentView(R.layout.activity_game)
        initLayouts()
        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(R.string.access_token_key), null)
        accessToken = accessToken.toString()
        myIntent = intent
        bundle = Bundle()
        bundle = myIntent.extras as Bundle
        getIncomingIntent()
        initConstants()

        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)

        startGame(this, metrics)
    }

    /**
     * Initializes all property layouts that are lateinited
     * */
    private fun initLayouts() {
        gridRack = GridLayout(this)
        gridRack.rowCount = 1
        gridRack.columnCount = 7
        savedImageViewsRack = MutableList(7) { ImageView(this) }
        linearLayoutsRack = Array(7) { LinearLayout(this) }
        imageViewsRack = Array(7) { ImageView(this) }

        imageViewsBoard = Array(225) { ImageView(this) }
        linearLayouts = Array(225) { LinearLayout(this) }
        savedImageView = ImageView(this)
        savedLinearLayout = LinearLayout(this)
        savedPlayerTiles = mutableListOf()

        gridBoard = GridLayout(this)
        gridBoard.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        gridBoard.orientation = GridLayout.HORIZONTAL
        gridBoard.rowCount = 15
        gridBoard.columnCount = 15

        savedImageView.tag = null
        savedImageView.setImageResource(0)

        frameLayoutBoard = FrameLayout(this)
        val flparams = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.MATCH_PARENT,
            Gravity.BOTTOM
        )

        frameLayoutBoard.layoutParams = flparams

        boardImageView = ImageView(this)
        boardImageView.setImageResource(R.drawable.board)
        boardImageView.scaleType = ImageView.ScaleType.FIT_XY

        //created in xml
        layoutgameboardandrack.orientation = LinearLayout.VERTICAL

        backButton = findViewById(R.id.backbutton)
        layButton = findViewById(R.id.laybutton)
        chatButton = findViewById(R.id.chatbutton)

    }

    /**
     * initializes bearer token
     * rest calls my players username
     * gets scores and names from my player and other players from bundle
     * sets textviews playernames and scores
     * disables and hide skip, back and laybutton when it is not my players turn
     * */
    private fun getIncomingIntent() {
        bundle.javaClass
        GlobalScope.launch(Dispatchers.Main) {
            var responseUser: Response<User>? = null
            withContext(Dispatchers.Default) {
                try {
                    responseUser = authService.getUserDetails(accessToken as String).execute()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            var game: Game? = null
            withContext(Dispatchers.Default) {
                val response = gameService.getGameById(accessToken as String, bundle.get("id") as String).execute()
                if (!response.isSuccessful) {
                    throw HttpException(response)
                }
                game = response.body()
            }

            myUsername = responseUser?.body()?.username
            Log.d("myUsername", responseUser?.body()?.username)

            GameTitleGame.text = game?.title
            Log.d(localClassName + " bundletitle", GameTitleGame.text.toString())
            val playerNames = game?.players?.map { it.username }
            val indexMyPlayer = playerNames?.indexOf(myUsername)

            //val indexMyPlayer = bundle.get("indexmyplayer") as String
//            val playersString: String = bundle.get("playerss") as String
//            val playersList: List<String> = playersString.split(",").map { it.trim() }

            val opponentPlayersList = playerNames?.filter { it != myUsername } ?: emptyList()
            //val scores = bundle.get("scores") as String

            val scores: List<Score> = game?.scores ?: emptyList()
            val opponentScoresList = scores.map { it.score.toString() }.toMutableList()
            opponentScoresList.removeAt(indexMyPlayer as Int)

            when (playerNames.size) {
                2 -> {
                    tvgameplayer2name.text = opponentPlayersList[0]
                    tvgameplayer2points.text = opponentScoresList[0]
                }
                3 -> {
                    tvgameplayer2name.text = opponentPlayersList[0]
                    tvgameplayer2points.text = opponentScoresList[0]
                    tvgameplayer4name.text = opponentPlayersList[1]
                    tvgameplayer4points.text = opponentScoresList[1]
                }
                4 -> {
                    tvgameplayer2name.text = opponentPlayersList[0]
                    tvgameplayer2points.text = opponentScoresList[0]
                    tvgameplayer3name.text = opponentPlayersList[1]
                    tvgameplayer3points.text = opponentScoresList[1]
                    tvgameplayer4name.text = opponentPlayersList[2]
                    tvgameplayer4points.text = opponentScoresList[2]
                }
            }

            //currentlyPlaying = bundle.get("currentlyPlaying") as String
            currentlyPlaying = playerNames[game?.currentlyPlaying as Int]

            for (i in 2 until 4) {
                val textViewId = "tvgameplayer${i}name"
                val textViewIdScore = "tvgameplayer${i}points"
                val resTextViewId = resources.getIdentifier(textViewId, "id", packageName)
                val resTextViewScoreId = resources.getIdentifier(textViewIdScore, "id", packageName)
                val playerText = findViewById<TextView>(resTextViewId)
                val playerTextScore = findViewById<TextView>(resTextViewScoreId)
                if (playerText.text == currentlyPlaying) {
                    playerText.setTypeface(playerText.typeface, Typeface.BOLD)
                    playerText.setTextColor(resources.getColor(R.color.default_button_color, theme))
                    playerTextScore.setTypeface(playerText.typeface, Typeface.BOLD)
                    playerTextScore.setTextColor(resources.getColor(R.color.default_button_color, theme))
                }
            }

            isMyTurn = currentlyPlaying == myUsername
            laybutton.isEnabled = isMyTurn
            skipbutton.isEnabled = isMyTurn
            backButton.isEnabled = isMyTurn

            if (!isMyTurn) {
                layButton.visibility = ViewGroup.GONE
                skipbutton.visibility = ViewGroup.GONE
                backButton.visibility = ViewGroup.GONE
            }

            val myScore = game?.scores?.filter { it.username == myUsername }?.first()
            //val myScore = bundle.get("myscore")
            scoreuser.text = myScore?.score.toString()
        }
    }

    /**
     * initializes clicklistener for 225 Imageviews
     * enables logic to save "Tile" by pressing on it and removing the saved tile while pressing
     * on blank space in the board. same logic can be found in initRackClickListAndLayouts
     * initializes wrapper layouts and add layouts to parent layout: a gridlayout.
     * */
    private fun initBoardClickListAndLayouts(metrics: DisplayMetrics) {
        for (i in 0 until imageViewsBoard.size) {


            imageViewsBoard[i].setOnClickListener { v ->

                //if pressed on tile
                if (v.tag is TileBinding) {
                    if (v.tag == savedImageView.tag) {
                        Log.d("INFO", "if, if. iv already clicked")
                        // iv already clicked
                    } else {
                        savedImageView.tag = v.tag as TileBinding
                        savedLinearLayout = linearLayouts[i]
                        Log.d("INFO", "if, else. pressed on tile, saved. tag: " + v.tag.toString())
                    }

                    //if pressed in white
                } else {
                    if (savedImageView.tag != null) {
                        Log.d("INFO", "else, if. pressed in white and settag and draw")
                        //set tag of iv[i] to saved tag
                        val tileBinder = savedImageView.tag as TileBinding
                        tileBinder.pos = i
                        imageViewsBoard[i].tag = savedImageView.tag
                        //set iv[i] to saved drawable

                        savedPlayerTiles.add(tileBinder)
                        imageViewsBoard[i].setImageResource(tileBinder.drawable as Int)

                        linearLayouts[i].removeAllViews()
                        linearLayouts[i].addView(imageViewsBoard[i])

                        savedLinearLayout.removeAllViews()
                        savedImageView.tag = null
                    } else {
                        Log.d("INFO", "else, else.  pressed in white and doesnt do anything")
                        //Log.d("INFO: ", "else,else" + imageViewsBoard[111].getTag(R.id.TAG_TILE_ID))

                    }
                }
                return@setOnClickListener
            }
            linearLayouts[i].layoutParams =
                ViewGroup.LayoutParams(
                    metrics.widthPixels / 15,
                    metrics.widthPixels / 15
                )
            linearLayouts[i].addView(imageViewsBoard[i], metrics.widthPixels / 15, metrics.widthPixels / 15)
            gridBoard.addView(
                linearLayouts[i],
                ViewGroup.LayoutParams(metrics.widthPixels / 15, metrics.widthPixels / 15)
            )
        }

    }

    /**
     * same as initBoardClickListandLayout, but for the letterrack
     * */
    private fun initRackClickListAndLayouts(metrics: DisplayMetrics) {

        linearLayoutsRack.forEach { it.removeAllViews() }
        gridRack.removeAllViews()

        for (i in 0 until lettersOnRack.size) {
            imageViewsRack[i].tag = lettersOnRack[i]
            if (lettersOnRack.isNotEmpty()) {
                imageViewsRack[i].setImageResource(lettersOnRack[i].drawable as Int)
            }
            if (isMyTurn) {
                imageViewsRack[i].setOnClickListener { v ->

                    //if pressed on tile
                    if (v.tag is TileBinding) {
                        if (v.tag == savedImageView.tag) {
                            Log.d("INFO: ", "if,if")
                            // iv already clicked
                        } else {
                            savedImageView.tag = v.tag as TileBinding

                            savedLinearLayout = linearLayoutsRack[i]
                            Log.d("INFO: ", "if,else")
                        }
                        //if pressed in white
                    } else {
                        if (savedImageView.tag != null) {
                            Log.d("INFO: ", "else,if")
                            //set tag of iv[i] to saved tag
                            val tileBinderRackTile = savedImageView.tag as TileBinding
                            imageViewsRack[i].tag = savedImageView.tag

                            //set iv[i] to saved drawable
                            imageViewsRack[i].setImageResource(tileBinderRackTile.drawable as Int)

                            savedLinearLayout.removeAllViews()
                            savedImageView.tag = null
                        } else {
                            Log.d("INFO: ", "else,else")

                        }
                    }
                    return@setOnClickListener
                }
            }
            linearLayoutsRack[i].layoutParams =
                ViewGroup.LayoutParams(
                    metrics.widthPixels / 7,
                    metrics.widthPixels / 7
                )

            linearLayoutsRack[i].addView(imageViewsRack[i], metrics.widthPixels / 7, metrics.widthPixels / 7)
            gridRack.columnCount = 7
            gridRack.addView(
                linearLayoutsRack[i],
                ViewGroup.LayoutParams(metrics.widthPixels / 7, metrics.widthPixels / 7)
            )
        }

    }

    /**
     * addViews to different parentlayouts
     * calling methods for the back, lay and skip buttons
     * */
    private fun fillParentViews(metrics: DisplayMetrics) {

        frameLayoutBoard.addView(
            boardImageView, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                metrics.widthPixels
            )
        )
        frameLayoutBoard.addView(
            gridBoard, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                metrics.widthPixels
            )
        )
        //defined in xml
        layoutgameboardandrack.addView(
            frameLayoutBoard, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                metrics.widthPixels
            )
        )
        //defined in xml
        llgridrack.addView(
            gridRack, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, metrics.widthPixels / 7
            )
        )

        backButtonClickList()
        layButtonClickList(metrics)
        skipButtonClickList()
    }

    /**
     * logic for laybutton
     * generates for every tile its x and y coordinate from an imageViewBoard Array of 225
     * @return generates a dto with tiles laid on board
     * */
    private fun reverseLaidTiles() {
        val laidTiles = imageViewsBoard
            .filter { it1 -> it1.tag != null }
            .filter { it1 -> (it1.tag as TileBinding).isRackTile }


        for (i in 0 until laidTiles.size) {
            imageViewsBoard[(laidTiles[i].tag as TileBinding).pos].setImageDrawable(null)
            linearLayouts[(laidTiles[i].tag as TileBinding).pos].removeView(laidTiles[i])
            linearLayouts[(laidTiles[i].tag as TileBinding).pos].addView(laidTiles[i])
            imageViewsBoard[(laidTiles[i].tag as TileBinding).pos].tag = null
        }

        imageViewsRack.forEach {
            it.setImageResource(0)
            it.tag = null
        }
        for (i in 0 until lettersOnRack.size) {
            imageViewsRack[i].tag = lettersOnRack[i]
            if (lettersOnRack.isNotEmpty()) {
                imageViewsRack[i].setImageResource(lettersOnRack[i].drawable as Int)
                linearLayoutsRack[i].removeView(imageViewsRack[i])
                linearLayoutsRack[i].addView(imageViewsRack[i])
            }
        }
    }

    /**
     * logic backbutton
     * when backbutton is pressed or there is an invalidTilePlacement in layButtonClickList
     * it resets the state of the board
     * and the rack before you laid any tile in your turn
     * */
    private fun checkLaidTiles(): MutableList<LaidDownTile> {
        val laidTiles = imageViewsBoard
            .filter { it.tag != null }
            .map { t -> t.tag as TileBinding }
            .filter { it.isRackTile }

        val requestTileList: MutableList<LaidDownTile> = mutableListOf()
        var y: Int
        var x: Int
        for (i in 0 until laidTiles.size) {
            y = laidTiles[i].pos / 15
            x = laidTiles[i].pos % 15
            requestTileList.add(
                LaidDownTile(
                    laidTiles[i].char,
                    x, y
                )
            )
        }
        return requestTileList
    }

    /**
     * calls reverseLaidTiles when backbutton is clicked
     * */
    private fun backButtonClickList() {
        backButton.setOnClickListener {
            reverseLaidTiles()
        }
    }

    /**
     * sends two api calls and disables buttons according if first succeed
     * call1 sends laid tiles to backend
     * if it succeeds call2 draws new tiles from letterbag
     * back, skip and laybutton are hidden, letterrack and score is updated
     * else: generates toast: invalid placement of tiles.
     * */
    private fun layButtonClickList(metrics: DisplayMetrics) {
        layButton.setOnClickListener {
            val laidTiles: MutableList<LaidDownTile> = checkLaidTiles()
            GlobalScope.launch(Dispatchers.Main) {
                var isValidPlacement = true
                var asyncPlayedTurn: PlayedTurn? = null
                withContext(Dispatchers.Default) {
                    val response =
                        gameService.placeTiles(
                            accessToken as String,
                            bundle.getString("id") as String,
                            PlaceTilesRequest(
                                laidTiles
                            )
                        ).execute()
                    if (!response.isSuccessful) {
                        if (response.code() == 403) {
                            isValidPlacement = false
                        } else {
                            throw HttpException(response)
                        }

                    }
                    asyncPlayedTurn = response.body()
                }

                if (isValidPlacement) {
                    laybutton.isEnabled = false
                    skipbutton.isEnabled = false
                    backButton.isEnabled = false

                    imageViewsBoard.forEach { it.setOnClickListener(null) }
                    imageViewsRack.forEach { it.setOnClickListener(null) }

                    var tempRack: LetterRack? = null
                    withContext(Dispatchers.Default) {
                        val response =
                            letterRackService.getRandomTiles(
                                accessToken as String,
                                bundle.get("id") as String,
                                drawTilesRequest = DrawTilesRequest(laidTiles.size)
                            ).execute()
                        if (!response.isSuccessful) {
                            throw HttpException(response)
                        }
                        tempRack = response.body()
                    }

                    val oldScore = scoreuser.text.toString().toInt()
                    val receivedScore = asyncPlayedTurn?.words?.map { t -> t.score }?.reduce { acc, i -> acc + i } ?: 0
                    val newScore = receivedScore + oldScore
                    scoreuser.text = newScore.toString()

                    lettersOnRack.removeAll(lettersOnRack)
                    lettersOnRack.addAll(tempRack?.tiles?.map {
                        TileBinding(
                            char = it.char,
                            drawable = getDrawableFromChar(it.char, this@GameActivity.applicationContext),
                            isRackTile = true
                        )
                    } ?: emptyList())

                    initRackClickListAndLayouts(metrics)

                    imageViewsBoard.filter { it.tag != null }
                        .map { t -> t.tag as TileBinding }
                        .filter { it.isRackTile }
                        .forEach { it.isRackTile = false }

                    layButton.visibility = ViewGroup.GONE
                    skipbutton.visibility = ViewGroup.GONE
                    backButton.visibility = ViewGroup.GONE

                } else {
                    Toast.makeText(
                        this@GameActivity.applicationContext,
                        "Invalid placement of tiles.",
                        Toast.LENGTH_SHORT
                    ).show()

                    reverseLaidTiles()
                }
            }
        }

    }


    private fun showPopup(view: View) {
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.activity_skip_options_layout, null)

        val width: Int = ConstraintLayout.LayoutParams.WRAP_CONTENT
        val height: Int = ConstraintLayout.LayoutParams.WRAP_CONTENT
        val focusable = true
        val window = PopupWindow(popupView, width, height, focusable)
        window.showAtLocation(view, Gravity.CENTER, 0, 0)
    }

    /**
     * when skip is pressed current letterrack tiles will be deleted and replaced by
     * new ones from letterrack
     * */
    private fun skipButtonClickList() {
        skipbutton.setOnClickListener {
            showPopup(window.peekDecorView())

        }
    }

    private fun removeViewsParentViews() {
        llgridrack.removeAllViews()
        layoutgameboardandrack.removeAllViews()
        frameLayoutBoard.removeAllViews()
    }

    private fun getDrawableFromChar(char: Char, context: Context, language: String = "en_us"): Int {
        return context.resources.getIdentifier(
            char.toString().toLowerCase().replace(".", "blank") +
                    "_" +
                    language.toLowerCase().replace("-", "_"),
            "drawable",
            context.packageName
        )
    }

    /**
     *calls startgame api. calls letters from players letterrack
     * calls methods to initialize clicklisteners board and letterrack
     * fillParentViews and fill board with already played tiles
     */
    private fun startGame(context: Context, metrics: DisplayMetrics) {
        GlobalScope.launch(Dispatchers.Main) {
            if (!(bundle.get("hasStarted") as Boolean)) {
                withContext(Dispatchers.Default) {
                    val response =
                        gameService.startGame(accessToken as String, bundle.get("id") as String).execute()
                    if (!response.isSuccessful) {
                        throw HttpException(response)
                    }
                }
            }

            var tempRack: LetterRack? = null
            withContext(Dispatchers.Default) {
                val response =
                    letterRackService.getLetterRack(accessToken as String, bundle.get("id") as String).execute()
                tempRack = response.body()

                if (!response.isSuccessful) {
                    throw HttpException(response)
                }
            }

            Log.d(localClassName + " temprack", tempRack.toString())

            if (tempRack != null) {
                lettersOnRack.removeAll(lettersOnRack)
                lettersOnRack.addAll(tempRack?.tiles?.map
                { t ->
                    TileBinding(
                        char = t.char,
                        isRackTile = true,
                        pos = -1,
                        drawable = getDrawableFromChar(t.char, applicationContext, "en_us")
                    )
                } as List)
            }

            removeViewsParentViews()

            for (i in 0 until (tempRack?.tiles?.size ?: 0)) {
                if (tempRack?.tiles?.get(i) != null) {
                    imageViewsRack[i].tag = TileBinding(
                        char = tempRack?.tiles?.get(i)?.char as Char,
                        pos = -1,
                        isRackTile = true,
                        drawable = getDrawableFromChar(tempRack?.tiles?.get(i)?.char as Char, context)
                    )
                    imageViewsRack[i].setImageResource(
                        getDrawableFromChar(
                            tempRack?.tiles?.get(i)?.char as Char,
                            context
                        )
                    )
                }
            }

            //
            for (i in 0 until imageViewsBoard.size) {
                imageViewsBoard[i].setImageDrawable(null)
                linearLayouts[i].removeAllViews()
                imageViewsBoard[i].tag = null
            }
            var game: Game? = null
            withContext(Dispatchers.Default) {
                val response = gameService.getGameById(accessToken as String, bundle.get("id") as String).execute()
                if (!response.isSuccessful) {
                    throw HttpException(response)
                }
                game = response.body()
            }

            initRackClickListAndLayouts(metrics)
            initBoardClickListAndLayouts(metrics)
            fillParentViews(metrics)

            fillBoard(game)

        }
    }

    private fun fillBoard(game: Game?) {

        val tileBinderBoard = mutableListOf<TileBinding>()
        val boardString = game?.board
        for (i in 0 until boardString?.length as Int) {
            linearLayouts[i].removeAllViews()
            tileBinderBoard.add(
                TileBinding(
                    char = boardString[i],
                    isRackTile = false,
                    drawable = getDrawableFromChar(boardString[i], applicationContext),
                    pos = i,
                    isEmpty = boardString[i] == '_'
                )
            )
        }

        for (i in 0 until tileBinderBoard.size) {
            if (tileBinderBoard[i].char == '_') {
                linearLayouts[i].addView(imageViewsBoard[i])
            } else {
                imageViewsBoard[i].tag = tileBinderBoard[i]
                imageViewsBoard[i].setImageResource(tileBinderBoard[i].drawable as Int)
                linearLayouts[i].addView(imageViewsBoard[i])
            }
        }

    }

    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        letterRackService = retrofit.create(LetterRackService::class.java)
        gameService = retrofit.create(GameService::class.java)
        authService = retrofit.create(AuthService::class.java)
    }

}