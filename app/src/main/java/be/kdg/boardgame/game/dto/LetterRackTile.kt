package be.kdg.boardgame.game.dto

data class LetterRackTile(val id: String? = "", val char: Char = '_')
