package be.kdg.boardgame.game.dto

data class PlayedTurn (
    val words: List<WordScore> = mutableListOf() ,
    val representation: String = "",
    val board: String = "",
    val over: Boolean = false
)