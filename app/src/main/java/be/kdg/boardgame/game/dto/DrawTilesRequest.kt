package be.kdg.boardgame.game.dto

data class DrawTilesRequest(val amount: Int = 0)