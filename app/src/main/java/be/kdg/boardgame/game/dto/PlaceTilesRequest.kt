package be.kdg.boardgame.game.dto

data class PlaceTilesRequest (
    val tiles: List<LaidDownTile> = mutableListOf()
)
