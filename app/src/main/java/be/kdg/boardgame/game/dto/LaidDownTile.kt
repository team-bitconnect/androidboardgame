package be.kdg.boardgame.game.dto

data class LaidDownTile (
    val char: Char = '<',
    val x: Int = -1,
    val y: Int = -1
)