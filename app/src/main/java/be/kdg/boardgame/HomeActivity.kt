package be.kdg.boardgame

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import be.kdg.boardgame.authentication.LoginActivity
import be.kdg.boardgame.authentication.services.AuthService
import be.kdg.boardgame.gamecreation.NewGameActivity
import be.kdg.boardgame.gamecreation.dto.User
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

class HomeActivity : BaseNavigationActivity() {

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private var accessToken: String? = null
    private lateinit var authService: AuthService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(R.string.access_token_key), null)
        this.initBottomNavBar(R.id.action_home, bottom_navigation, this)
        val btnNewGame: Button = findViewById(R.id.btnNewGame)
        val btnLogout: Button = findViewById(R.id.btnLogout)
        initConstants()

        btnNewGame.setOnClickListener {
            startActivity(Intent(this, NewGameActivity::class.java))
        }

        btnLogout.setOnClickListener {
            logout()
        }
    }




    private fun logout() {
        //Facebook logout
        if (AccessToken.getCurrentAccessToken() != null) {
            GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/permissions/",
                null,
                HttpMethod.DELETE,
                GraphRequest.Callback {
                    AccessToken.setCurrentAccessToken(null)
                    LoginManager.getInstance().logOut()
                    finish()
                }).executeAsync()
        }
        //Google logout
        mGoogleSignInClient.signOut()
            .addOnCompleteListener(this) {
                // ...
                Toast.makeText(applicationContext, "Logged Out", Toast.LENGTH_SHORT).show()
                val i = Intent(applicationContext, LoginActivity::class.java)
                startActivity(i)
            }
    }

    override fun onStart() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        super.onStart()
    }

    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        authService = retrofit.create(AuthService::class.java)
    }
}
