package be.kdg.boardgame

import android.content.Context
import android.content.Intent
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import be.kdg.boardgame.overview.OverviewActivity
import be.kdg.boardgame.friends.AddFriendsActivity
import be.kdg.boardgame.notifications.NotificationsActivity
import be.kdg.boardgame.settings.SettingsActivity

open class BaseNavigationActivity : AppCompatActivity() {
    public override fun onPause() {
        super.onPause()
        overridePendingTransition(0, 0)
    }
     fun initBottomNavBar(itemIdIgnore: Int, bottomNavigationView: BottomNavigationView, context: Context) {
        bottomNavigationView.selectedItemId = itemIdIgnore
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                itemIdIgnore -> {
                }
                R.id.action_overview -> {
                    context.startActivity(Intent(context, OverviewActivity::class.java))
                }
                R.id.action_notifications -> {
                    context.startActivity(Intent(context, NotificationsActivity::class.java))
                }
                R.id.action_home -> {
                    context.startActivity(Intent(context, HomeActivity::class.java))
                }
                R.id.action_friends -> {
                    context.startActivity(Intent(context, AddFriendsActivity::class.java))
                }
                R.id.action_usersettings -> {
                    context.startActivity(Intent(context, SettingsActivity::class.java))
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }
}