package be.kdg.boardgame.overview.services

import be.kdg.boardgame.overview.dto.OverviewGame
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface OverviewService {

    @GET("api/overview")
    fun games(
        @Header("Authorization") authHeader: String
    ): Call<List<OverviewGame>>
}