package be.kdg.boardgame.overview

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import be.kdg.boardgame.BaseNavigationActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.overview.adapters.OverviewAdapter
import be.kdg.boardgame.overview.dto.OverviewGame
import be.kdg.boardgame.overview.services.OverviewService
import kotlinx.android.synthetic.main.activity_overview.*
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import kotlin.coroutines.CoroutineContext


class OverviewActivity : BaseNavigationActivity(), CoroutineScope {
    private var mGames = ArrayList<OverviewGame>()
    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private var accessToken: String? = null
    private lateinit var overviewService: OverviewService
    private lateinit var mJob: Job
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main
    private lateinit var adapter: OverviewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_overview)
        initConstants()
        adapter = OverviewAdapter(this, mGames)
        initRecyclerView()

        val intent = intent
        val bundle = intent.extras
        var gameCreated: Boolean? = false
        var gameCreatedResponse: String? = ""
        if (bundle != null) {
            gameCreated = bundle.get("gamecreated") as Boolean?
            gameCreatedResponse = bundle.get("gamecreatederror") as String?
            if (gameCreated != null)
                if (gameCreated) {
                    Toast.makeText(this, "game is created. Wait a minute for the game to appear.", Toast.LENGTH_LONG)
                        .show()
                } else {
                    Toast.makeText(this, "game is not created: $gameCreatedResponse", Toast.LENGTH_LONG).show()
                }
        }

        initBottomNavBar(R.id.action_overview, bottom_navigation, this)
        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(R.string.access_token_key), null)

        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.Default) {
                val bearer = accessToken.toString()
                val response = overviewService.games(bearer).execute()
                val games: List<OverviewGame> = response.body() ?: emptyList()

                mGames.addAll(games)
                if (response.isSuccessful) {

                } else {
                    throw HttpException(response)
                }
            }

            Log.d("INFO", mGames.toString())
            Log.d("INFO", accessToken)
            initRecyclerView()
        }

        onRefresh()
    }


    private fun initRecyclerView() {
        val recyclerView: RecyclerView = findViewById(R.id.game_overview_listview)
        mGames.reverse()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        overviewService = retrofit.create(OverviewService::class.java)
    }

    private fun onRefresh() {
        swiperefresh.setOnRefreshListener {
            GlobalScope.launch(Dispatchers.Main) {
                val bearer = accessToken.toString()
                var games: List<OverviewGame> = emptyList()
                var response: Response<List<OverviewGame>>? = null

                GlobalScope.async {
                    response = overviewService.games(bearer).execute()
                }.await()

                games = response?.body() ?: emptyList()
                mGames.removeAll{true}
                mGames.addAll(games)
                if (response?.isSuccessful == true) {

                } else {
                    throw HttpException(response)
                }
                val recyclerView: RecyclerView = findViewById(R.id.game_overview_listview)
                mGames.reverse()
                adapter.mItemList = mGames
                recyclerView.adapter = adapter
                recyclerView.layoutManager = LinearLayoutManager(this@OverviewActivity)
            }
            swiperefresh.isRefreshing = false
            adapter.notifyDataSetChanged()
        }

    }
}

