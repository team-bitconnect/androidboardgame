package be.kdg.boardgame.overview.services

import be.kdg.boardgame.gamecreation.dto.Game
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface OverviewDetailService {
    @GET("api/games/{id}")
    fun gameDetail(
        @Header("Authorization") authHeader: String,
        @Path("id") id: String
    ): Call<Game>

    @GET("api/games/{gameId}/start")
    fun startGame(
        @Header("Authorization") authHeader: String,
        @Path("gameId") id: String
    ): Call<Boolean>
}
