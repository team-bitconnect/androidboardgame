package be.kdg.boardgame.overview

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import be.kdg.boardgame.BaseNavigationActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.authentication.services.AuthService
import be.kdg.boardgame.game.GameActivity
import be.kdg.boardgame.gamecreation.dto.Game
import be.kdg.boardgame.gamecreation.dto.User
import be.kdg.boardgame.overview.services.OverviewDetailService
import be.kdg.boardgame.replay.ReplayActivity
import kotlinx.android.synthetic.main.content_game_overview_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


class OverviewDetailActivity : BaseNavigationActivity() {

    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private lateinit var overviewDetailService: OverviewDetailService
    private lateinit var authService: AuthService

    private var accessToken: String? = null
    private var id: String = ""
    private var intentGameAct: Intent? = null
    private lateinit var className: String
    private var username: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        className = this@OverviewDetailActivity.localClassName
        initConstants()
        setContentView(R.layout.content_game_overview_detail)
        initBottomNavBar(R.id.action_overview, bottom_navigation, this)
        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(R.string.access_token_key), null)
        intentGameAct = Intent(applicationContext, GameActivity::class.java)
        PlayGameDetailBtn.isEnabled = false
        getIncomingIntent()
    }

    private fun getIncomingIntent() {
        val intent = intent
        val bundle = intent.extras



        var playerNamesList: List<String> = listOf()
        if (bundle != null) {
            id = bundle.get("id") as String
            overviewdetailtitle.text = bundle.get("title") as String?
            playerNamesList = bundle.get("players").toString().split(",").map { it.trim() }
            //playernames.text = players.map { "$it, " }.reduce { acc, s -> acc + s }.trimEnd(',', ' ')
            playernames.text = playerNamesList.joinToString(", ")
            val curPlaying: String? = bundle.get("currentlyPlaying") as String
            currentlyplaying.text =
                applicationContext.getString(R.string.turntext, playerNamesList[curPlaying?.toInt() ?: 0])
        }

        var indexMyPlayer = 0
        var game: Game? = null
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.Default) {
                val bearer = accessToken.toString()
                val response = overviewDetailService.gameDetail(bearer, bundle.get("id") as String).execute()
                game = response.body()
                if (!response.isSuccessful) throw HttpException(response)
            }
            PlayGameDetailBtn.isEnabled = true
            Log.d("$className - INFO: gameId", "${game?.id}")
            var responseUser: Response<User>? = null
            withContext(Dispatchers.Default) {
                try {
                    responseUser = authService.getUserDetails(accessToken as String).execute()

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            username = responseUser?.body()?.username
            Log.d("username", responseUser?.body()?.username ?: "null")

            indexMyPlayer = playerNamesList.indexOf(username)
        }



        PlayGameDetailBtn.setOnClickListener {
            intentGameAct?.putExtra("title", game?.title)
            intentGameAct?.putExtra("playerss", bundle.get("players").toString())
            intentGameAct?.putExtra("currentlyPlaying", game?.currentlyPlaying.toString())
            intentGameAct?.putExtra("id", bundle.get("id") as String)
            intentGameAct?.putExtra("isOver", game?.isOver)
            intentGameAct?.putExtra("hasStarted", game?.hasStarted)

            if (game?.scores?.size!! > 0) {
                intentGameAct?.putExtra(
                    "scores", game?.scores?.map { it.score }?.joinToString(",")
                )
                intentGameAct?.putExtra("myscore", game?.scores?.map { it.score }?.get(indexMyPlayer))
            } else {
                intentGameAct?.putExtra(
                    "scores", "0,0"
                )
                intentGameAct?.putExtra("myscore", "0")
            }
            intentGameAct?.putExtra("indexmyplayer", indexMyPlayer.toString())
            intentGameAct?.putExtra("board", game?.board)
            startActivity(intentGameAct)

            /*  GlobalScope.launch(Dispatchers.Main) {
                  withContext(Dispatchers.Default) {
                      val response =
                          overviewDetailService.startGame(accessToken.toString(), id)
                              .execute()
                      if (response.isSuccessful) {
                          if (response.body() == true) {
                              Log.d(
                                  "$className - INFO",
                                  "game is being started. response: " + response.body()
                              )
                          } else {
                              Log.d(
                                  "$className - INFO",
                                  "game already started. response: " + response.body()
                              )
                          }
                      } else {
                          Log.d(
                              "$className - INFO",
                              "unsuccessful startGame. response: " + response.errorBody()
                          )
                      }
                  }
                  startActivity(intentGameAct)
              }*/

        }

        replayGameDetailBtn.setOnClickListener {
            val replayIntent = Intent(applicationContext, ReplayActivity::class.java)
            replayIntent.putExtra("gameId", id)
            startActivity(replayIntent)
        }
    }


    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        overviewDetailService = retrofit.create(OverviewDetailService::class.java)
        authService = retrofit.create(AuthService::class.java)
    }
}
