package be.kdg.boardgame.overview.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import be.kdg.boardgame.R
import be.kdg.boardgame.overview.OverviewDetailActivity
import be.kdg.boardgame.overview.dto.OverviewGame


class OverviewAdapter(
    private var mContext: Context,
    var mItemList: MutableList<OverviewGame> = mutableListOf()

) : RecyclerView.Adapter<OverviewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.layout_overview_listitems, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mItemList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val players = mItemList[position].players
        Log.d(javaClass.simpleName+" players",players.toString())
        holder.playersTextView.text = players.joinToString(", ") { it.username }
        holder.titleTextView.text = mItemList[position].title

        // currentlyplaying
        val curPlayingIndex = mItemList[position].currentlyPlaying.toInt()
        holder.turnTextView.text = "Turn: ${mItemList[position].players[curPlayingIndex].username}"

        val intent = Intent(mContext, OverviewDetailActivity::class.java)

        intent.putExtra("title", mItemList[position].title)
        intent.putExtra("players",  players.joinToString(",") { it.username })
        intent.putExtra("currentlyPlaying", mItemList[position].currentlyPlaying)
        intent.putExtra("id", mItemList[position].id)

        holder.parentLayout.setOnClickListener {
            mContext.startActivity(intent)
        }
    }

    data class ViewHolder(
        var itemView: View,
        var titleTextView: TextView = itemView.findViewById(R.id.listitem_game_name),
        var turnTextView: TextView = itemView.findViewById(R.id.listitem_game_turn),
        var playersTextView: TextView = itemView.findViewById(R.id.listitem_game_players),
        var parentLayout: ConstraintLayout = itemView.findViewById(R.id.listitem_layout_overview)
    ) : RecyclerView.ViewHolder(itemView)
}