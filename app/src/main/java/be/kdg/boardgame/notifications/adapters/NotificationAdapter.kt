package be.kdg.boardgame.notifications.adapters

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import be.kdg.boardgame.R
import be.kdg.boardgame.game.GameActivity
import be.kdg.boardgame.game.models.AcceptInvitation
import be.kdg.boardgame.game.services.GameService
import be.kdg.boardgame.notifications.dto.Notification
import be.kdg.boardgame.notifications.services.NotificationService
import be.kdg.boardgame.overview.services.OverviewService
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import android.app.Activity
import android.support.v4.widget.SwipeRefreshLayout
import be.kdg.boardgame.notifications.NotificationsActivity
import kotlinx.android.synthetic.main.activity_notifications.*


class NotificationAdapter(
    private var mContext: Context,
    var notifications: MutableList<Notification> = mutableListOf(),
    var recyclerView: RecyclerView
) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private lateinit var notificationService: NotificationService
    private lateinit var gameService: GameService
    private var accessToken: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initConstants()
        sharedPref = mContext.getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(mContext.getString(R.string.access_token_key), null)
        var view = LayoutInflater.from(parent.context).inflate(R.layout.layout_notifications_listitems, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val notif = notifications[position]
        holder.titleTextView.text = "This is a notification"
        holder.contentTextView.text = "There's nothing here"
        Log.d(javaClass.simpleName + " notifications", notif.toString())

        val intent = Intent(mContext, GameActivity::class.java)
        val df = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(notif.date)
        intent.putExtra("id", "noid")
        holder.titleTextView.text = notif.notifTitle
        holder.contentTextView.text = notif.content
        holder.dateTextView.text = df

        // go to game by gameid when clicking on notif
        if (notif.notifTitle.toLowerCase().contains("turn")
            || notif.notifTitle.toLowerCase().contains("word")
            || notif.notifTitle.toLowerCase().contains("message")
        ) {

            if (notif.notifTitle.toLowerCase().contains("turn")
                || notif.notifTitle.toLowerCase().contains("word")
            ) {
                holder.avatarImageView.setImageResource(R.drawable.ic_border_outer_black_24dp)
            }

            intent.putExtra("id", notif.gameId)
            intent.putExtra("hasStarted", true)

            holder.parentLayout.setOnClickListener { v ->
                GlobalScope.launch {
                    // delete notif on click
                    deleteNotification(notif.id)
                }
                v.context.startActivity(intent)
            }
        }
        val rootView: SwipeRefreshLayout =
            (mContext as NotificationsActivity).window.decorView.findViewById(R.id.swiperefreshnotifications)
        // send accept to game by gameid when clicking on notif
        if (notif.notifTitle.toLowerCase().contains("invitation")) {
            Log.d(javaClass.simpleName + " if: ", "invitation message")
            holder.acceptImageButtonView.visibility = ViewGroup.VISIBLE
            holder.declineImageButtonView.visibility = ViewGroup.VISIBLE
            holder.avatarImageView.setImageResource(R.drawable.ic_border_outer_black_24dp)

            // accept/decline
            holder.acceptImageButtonView.setOnClickListener {
                GlobalScope.launch(Dispatchers.Main) {
                    // delete notif on decision
                    deleteNotification(notif.id)
                    it.context.startActivity(Intent(mContext, NotificationsActivity::class.java))
                    (mContext as NotificationsActivity).finish()
                    invitationDecision(notif, true)
                }
            }

            holder.declineImageButtonView.setOnClickListener {
                GlobalScope.launch(Dispatchers.Main) {
                    // delete notif on decision
                    deleteNotification(notif.id)
                    it.context.startActivity(Intent(mContext, NotificationsActivity::class.java))
                    (mContext as NotificationsActivity).finish()
                    invitationDecision(notif, false)
                }
            }
        }
    }

    private suspend fun deleteNotification(id: String) {
        withContext(Dispatchers.Default) {
            notificationService.deleteNotification(
                accessToken as String,
                id
            ).execute()
        }
    }

    private suspend fun invitationDecision(notif: Notification, answer: Boolean) {
        withContext(Dispatchers.Default) {
            gameService.handleInvitationResponse(
                accessToken as String,
                notif.gameId,
                AcceptInvitation(answer)
            ).execute()
        }
    }

    data class ViewHolder(
        var itemView: View,
        var titleTextView: TextView = itemView.findViewById(R.id.listitem_notif_title),
        var contentTextView: TextView = itemView.findViewById(R.id.listitem_notif_content),
        var dateTextView: TextView = itemView.findViewById(R.id.listitem_notif_date),
        var avatarImageView: ImageView = itemView.findViewById(R.id.listitem_notif_avatar),
        var acceptImageButtonView: ImageView = itemView.findViewById(R.id.acceptinv),
        var declineImageButtonView: ImageView = itemView.findViewById(R.id.declineinv),
        var parentLayout: ConstraintLayout = itemView.findViewById(R.id.listitem_layout_notif)
    ) : RecyclerView.ViewHolder(itemView)

    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(mContext.resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        gameService = retrofit.create(GameService::class.java)
        notificationService = retrofit.create(NotificationService::class.java)
    }

}