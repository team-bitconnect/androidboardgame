package be.kdg.boardgame.notifications.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Clock
import java.time.LocalDateTime
import java.util.Date

data class Notification(
    @JsonProperty
    val id: String = "",
    @JsonProperty
    val content: String = "",
    @JsonProperty
    val notifTitle: String = "",
    @JsonProperty
    val gameId: String = "",
    @JsonProperty
    val date: Date = Date(System.currentTimeMillis())
)