package be.kdg.boardgame.notifications.services

import be.kdg.boardgame.notifications.dto.Notification
import retrofit2.Call
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path


interface NotificationService {
    @GET("api/notifications")
    fun notifications(
        @Header("Authorization") authHeader: String
    ): Call<List<Notification>>

    @DELETE("api/notifications/{notifId}")
    fun deleteNotification(
        @Header("Authorization") authHeader: String,
        @Path("notifId") notifId: String
    ): Call<Boolean>
}