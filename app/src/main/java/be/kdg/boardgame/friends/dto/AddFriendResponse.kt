package be.kdg.boardgame.addfriends.dto

data class AddFriendResponse (
    val message: String = ""
)