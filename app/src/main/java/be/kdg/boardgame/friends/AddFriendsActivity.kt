package be.kdg.boardgame.friends

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import be.kdg.boardgame.BaseNavigationActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.addfriends.dto.FriendDto
import be.kdg.boardgame.friends.services.FriendService
import kotlinx.android.synthetic.main.activity_add_friends.*
import kotlinx.coroutines.*
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import kotlin.coroutines.CoroutineContext


class AddFriendsActivity : BaseNavigationActivity(), CoroutineScope {

    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private lateinit var friendService: FriendService
    private var accessToken: String? = null
    private lateinit var mJob: Job
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_friends)
        initConstants()
        initBottomNavBar(R.id.action_friends, bottom_navigation, this)
        addEventHandlers()
    }

    private fun addEventHandlers(){
        btnAddFriend.setOnClickListener {
            addFriend()
        }
    }

    private fun addFriend() = GlobalScope.launch {
        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(be.kdg.boardgame.R.string.access_token_key), null)

        val bearer = accessToken.toString()
        Log.i("accesToken",accessToken.toString())
        val friendToAdd = FriendDto(etAddFriend.text.toString())

        val response = friendService.addFriend(bearer, friendToAdd).execute()

        Log.d("AddFriend", response.code().toString())

        if (response.code() == 200) {
            val intent = Intent(this@AddFriendsActivity, AddFriendsActivity::class.java)
            startActivity(intent)
            this@AddFriendsActivity.runOnUiThread(Runnable { Toast.makeText(this@AddFriendsActivity, "Friend " + friendToAdd.username + " added!", Toast.LENGTH_SHORT).show() })
            Log.d("AddFriend", "Friend with username " + friendToAdd.username + " is added!")
        } else {
            this@AddFriendsActivity.runOnUiThread(Runnable { Toast.makeText(this@AddFriendsActivity, "User not found", Toast.LENGTH_SHORT).show() })
            val json = JSONObject(response.errorBody()?.string())
            Log.d("AddFriend", json.toString())
        }
    }

    private fun initConstants() {
        sharedPref = getPreferences(Context.MODE_PRIVATE)
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(be.kdg.boardgame.R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        friendService = retrofit.create(FriendService::class.java)
    }
}
