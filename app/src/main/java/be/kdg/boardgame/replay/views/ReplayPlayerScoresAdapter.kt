package be.kdg.boardgame.replay.views

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import be.kdg.boardgame.R

class ReplayPlayerScoresAdapter(context: Context, private val data: List<Pair<String, Int>>) :
    RecyclerView.Adapter<ReplayPlayerScoresAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = inflater.inflate(R.layout.recyclerview_player_score, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvUsername.text = data[position].first
        holder.tvScore.text = data[position].second.toString()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var tvUsername: TextView = itemView.findViewById(R.id.tv_username)
        var tvScore: TextView = itemView.findViewById(R.id.tv_score)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            // Do nothing
        }
    }
}