package be.kdg.boardgame.replay

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import be.kdg.boardgame.R
import be.kdg.boardgame.replay.dto.ReplayTurn
import be.kdg.boardgame.replay.services.ReplayService
import be.kdg.boardgame.replay.views.ReplayBoardAdapter
import be.kdg.boardgame.replay.views.ReplayPlayerScoresAdapter
import kotlinx.android.synthetic.main.activity_replay.*
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.lang.Math.abs

class ReplayActivity : AppCompatActivity() {

    private lateinit var retrofit: Retrofit
    private lateinit var replayService: ReplayService
    private lateinit var boardAdapter: ReplayBoardAdapter
    private lateinit var scoresAdapter: ReplayPlayerScoresAdapter
    private lateinit var turns: List<ReplayTurn>

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var accessToken: String
    private var currentTurn: Int = 0
    private val speeds: Array<Long> = arrayOf(2000, 1750, 1500, 1250, 1000, 750, 500, 250)
    private var selectedSpeed: Int = 3
    private var playing: Boolean = false

    private var gameId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_replay)
    }

    override fun onStart() {
        super.onStart()

        if (intent?.extras != null) {
            gameId = intent?.extras?.getString("gameId") ?: ""
        }

        initConstants()
        initViews()
        setEvents()
    }

    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        replayService = retrofit.create(ReplayService::class.java)

        sharedPreferences = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPreferences.getString(getString(R.string.access_token_key), "") ?: ""
    }

    private fun initViews() {
        rv_scores.layoutManager = LinearLayoutManager(this@ReplayActivity, LinearLayoutManager.HORIZONTAL, false)
        rv_board.layoutManager = GridLayoutManager(this@ReplayActivity, 15)

        sp_speed.setSelection(selectedSpeed)

        GlobalScope.launch(Dispatchers.Main) {
            runBlocking(Dispatchers.Default) {
                val request = replayService.getTurns(accessToken, gameId)
                    .execute()

                if (!request.isSuccessful) {
                    Log.w("REPLAY", request.errorBody()?.string())
                }

                turns = request.body() ?: emptyList()
            }

            sb_turns.max = turns.size - 1
            updateBoard()
        }
    }

    private fun setEvents() {
        btn_next.setOnClickListener {
            if (turns.isEmpty())
                return@setOnClickListener

            currentTurn++
            updateTurn()
        }

        btn_previous.setOnClickListener {
            if (turns.isEmpty())
                return@setOnClickListener

            currentTurn--
            updateTurn()
        }

        btn_play_pause.setOnClickListener {
            toggleTimer()
        }

        sb_turns.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                currentTurn = progress
                updateTurn()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                // Do nothing
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // Do nothing
            }

        })

        sp_speed.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Do nothing
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedSpeed = position
            }
        }
    }

    private fun toggleTimer() {
        playing = !playing

        btn_play_pause.setImageDrawable(
            ContextCompat.getDrawable(
                this,
                if (playing) R.drawable.baseline_pause_24 else R.drawable.baseline_play_arrow_24
            )
        )

        if (playing)
            startPlayer()
    }

    private fun startPlayer() {
        GlobalScope.launch(Dispatchers.Main) {
            while (playing) {
                // delay
                delay(speeds[selectedSpeed])

                // update turn
                currentTurn++
                updateTurn()

                // stop playing when last turn
                if (currentTurn + 1 >= turns.size)
                    toggleTimer()
            }
        }
    }

    private fun updateTurn() {
        if (turns.isEmpty()) return

        currentTurn %= turns.size
        currentTurn = abs(currentTurn)

        if (tv_turn.text != (currentTurn + 1).toString())
            tv_turn.text = (currentTurn + 1).toString()
        if (sb_turns.progress != currentTurn)
            sb_turns.progress = currentTurn

        updateScores()
        updateBoard()
    }

    private fun updateScores() {
        val playerScorePairs =
            if (turns.isNotEmpty()) {
                val players = turns.distinctBy { it.player }.size
                turns
                    .filter { it.turnCounter <= currentTurn + 1 && it.turnCounter > currentTurn + 1 - players }
                    .map { Pair(it.player, it.score) }
            } else emptyList()

        scoresAdapter = ReplayPlayerScoresAdapter(this@ReplayActivity, playerScorePairs)
        rv_scores.adapter = scoresAdapter
    }

    private fun updateBoard() {
        val chars = if (turns.isNotEmpty()) turns[currentTurn].board.toCharArray().toList() else List(15 * 15) { '_' }

        boardAdapter = ReplayBoardAdapter(this@ReplayActivity, chars)
        rv_board.adapter = boardAdapter
    }
}
