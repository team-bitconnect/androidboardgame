package be.kdg.boardgame.replay.services

import be.kdg.boardgame.replay.dto.ReplayTurn
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface ReplayService {
    @GET("api/games/{gameId}/turns")
    fun getTurns(
        @Header("Authorization") authHeader: String,
        @Path("gameId") gameId: String
    ): Call<List<ReplayTurn>>
}