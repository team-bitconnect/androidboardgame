package be.kdg.boardgame.replay.views

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import be.kdg.boardgame.R

class ReplayBoardAdapter(private var context: Context, private val data: List<Char>) :
    RecyclerView.Adapter<ReplayBoardAdapter.ViewHolder>() {

    private val multiplierGrid = arrayOf(
        3, 0, 0, -2, 0, 0, 0, 3, 0, 0, 0, -2, 0, 0, 3,
        0, 2, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, 2, 0,
        0, 0, 2, 0, 0, 0, -2, 0, -2, 0, 0, 0, 2, 0, 0,
        -2, 0, 0, 0, 2, 0, 0, -2, 0, 0, 2, 0, 0, 0, -2,
        0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0,
        0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0,
        0, 0, -2, 0, 0, 0, -2, 0, -2, 0, 0, 0, -2, 0, 0,
        3, 0, 0, -2, 0, 0, 0, 2, 0, 0, 0, -2, 0, 0, 3,
        0, 0, -2, 0, 0, 0, -2, 0, -2, 0, 0, 0, -2, 0, 0,
        0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, -3, 0,
        0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0,
        -2, 0, 0, 0, 2, 0, 0, -2, 0, 0, 2, 0, 0, 0, -2,
        0, 0, 2, 0, 0, 0, -2, 0, -2, 0, 0, 0, 2, 0, 0,
        0, 2, 0, 0, 0, -3, 0, 0, 0, -3, 0, 0, 0, 2, 0,
        3, 0, 0, -2, 0, 0, 0, 3, 0, 0, 0, -2, 0, 0, 3
    )
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = inflater.inflate(R.layout.recyclerview_board_tile, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val char = data[position]
        val background: Drawable? =
            when {
                // letter tiles
                char != '_' -> {
                    val id = context.resources.getIdentifier("${char.toLowerCase()}_en_us", "drawable", context.packageName)
                    ContextCompat.getDrawable(context, id)
                }
                // center star
                position == data.size / 2 -> ContextCompat.getDrawable(context, R.drawable.center_star)
                // multipliers
                else -> when (multiplierGrid[position]) {
                    -2 -> ContextCompat.getDrawable(context, R.drawable.double_letter)
                    -3 -> ContextCompat.getDrawable(context, R.drawable.triple_letter)
                    2 -> ContextCompat.getDrawable(context, R.drawable.double_word)
                    3 -> ContextCompat.getDrawable(context, R.drawable.triple_word)
                    else -> ContextCompat.getDrawable(context, R.color.md_white_1000)
                }
            }

        holder.imgView.background = background
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var imgView: ImageView = itemView.findViewById(R.id.img_tile)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            // Do nothing
        }
    }
}