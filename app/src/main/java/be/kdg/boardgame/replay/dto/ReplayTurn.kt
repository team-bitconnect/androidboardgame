package be.kdg.boardgame.replay.dto

import be.kdg.boardgame.gamecreation.dto.User

data class ReplayTurn(
    val gameId: String = "",
    val turnCounter: Int = 0,
    var board: String = Array(15 * 15) { '_' }.joinToString(separator = "") { it.toString() },
    val player: String = "",
    val score: Int = 0,
    val racks: String = "",
    val users: List<User> = emptyList()
)