package be.kdg.boardgame.settings.dto

import be.kdg.boardgame.gamecreation.dto.User

class Settings (
    var player: User,
    var language: String = "nl-BE",
    var receiveMobileNotifications: Boolean = true,
    var receiveMailNotification: Boolean = true,
    var isPublicStatistic: Boolean = true
)