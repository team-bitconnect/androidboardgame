package be.kdg.boardgame.settings

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.Toast
import be.kdg.boardgame.BaseNavigationActivity
import be.kdg.boardgame.HomeActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.settings.services.SettingsService
import kotlinx.android.synthetic.main.activity_add_friends.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.coroutines.*
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import kotlin.coroutines.CoroutineContext

class SettingsActivity : BaseNavigationActivity(), CoroutineScope {

    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private lateinit var settingsService: SettingsService
    private var accessToken: String? = null
    private lateinit var mJob: Job
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        initBottomNavBar(R.id.action_usersettings, bottom_navigation_settings, this)
        initConstants()
        loadSettings()
        addEventHandlers()
    }

    private fun addEventHandlers() {
        btnUpdateUsername.setOnClickListener {
            updateUsername()
        }
        btnUpdatePassword.setOnClickListener {
            updatePassword()
        }
    }

    private fun updateUsername() = GlobalScope.launch {
        if (!etUpdateUsername.text.toString().isEmpty()) {
            val bearer = accessToken.toString()
            val signUpForm =
                SignUpForm(etUpdateUsername.text.toString(), etUpdateEmail.text.toString(), "")
            val response = settingsService.updateUsername(bearer, signUpForm).execute()
            if (response.code() == 200) {
                Log.d("Update username", "OK!")
                this@SettingsActivity.runOnUiThread {
                    tvErrorSettings.visibility = View.INVISIBLE
                    Toast.makeText(this@SettingsActivity, "Username updated!", Toast.LENGTH_SHORT).show()
                }
            } else {
                val json = JSONObject(response.errorBody()?.string())
                Log.d("Update user", json.toString())
            }
        }
    }

    private fun updatePassword() = GlobalScope.launch {
        if (etUpdatePassword.text.toString() != etUpdateConfirmPassword.text.toString() ||
            etUpdatePassword.text.toString().isEmpty() ||
            etUpdateConfirmPassword.text.toString().isEmpty()
        ) {
            this@SettingsActivity.runOnUiThread {
                tvErrorSettings.visibility = View.VISIBLE
            }
        } else {
            val bearer = accessToken.toString()
            val signUpForm =
                SignUpForm("", etUpdateEmail.text.toString(), etUpdatePassword.text.toString())
            val response = settingsService.updatePassword(bearer, signUpForm).execute()
            if (response.code() == 200) {
                Log.d("Update password", "OK!")
                this@SettingsActivity.runOnUiThread {
                    tvErrorSettings.visibility = View.INVISIBLE
                    Toast.makeText(this@SettingsActivity, "Password updated!", Toast.LENGTH_SHORT).show()
                }
            } else {
                val json = JSONObject(response.errorBody()?.string())
                Log.d("Update user", json.toString())
            }
        }
    }


    private fun loadSettings() = GlobalScope.launch {
        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(be.kdg.boardgame.R.string.access_token_key), null)

        val bearer = accessToken.toString()
        val response: Response<SignUpForm> = settingsService.getUser(bearer).execute()
        if (response.isSuccessful) {
            val userData: SignUpForm? = response.body()
            this@SettingsActivity.runOnUiThread {
                etUpdateUsername.text = Editable.Factory.getInstance().newEditable(userData?.username)
                etUpdateEmail.text = Editable.Factory.getInstance().newEditable(userData?.email)
            }
        } else {
            Log.d("ERROR", response.errorBody().toString() + " code: " + response.code())
            throw HttpException(response)
        }
    }

    private fun initConstants() {
        sharedPref = getPreferences(Context.MODE_PRIVATE)
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(be.kdg.boardgame.R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        settingsService = retrofit.create(SettingsService::class.java)
    }
}