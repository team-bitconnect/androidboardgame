package be.kdg.boardgame.settings.services

import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.authentication.dto.SignUpResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface SettingsService {

    @GET("api/user")
    fun getUser(
        @Header("Authorization") authHeader: String
    ): Call<SignUpForm>

    @PUT("api/user/username")
    fun updateUsername(
        @Header("Authorization") authHeader: String,
        @Body signUpForm: SignUpForm
    ): Call<Any>

    @PUT("api/user/password")
    fun updatePassword(
        @Header("Authorization") authHeader: String,
        @Body signUpForm: SignUpForm
    ): Call<Any>
}