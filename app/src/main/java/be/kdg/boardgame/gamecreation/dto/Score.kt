package be.kdg.boardgame.gamecreation.dto

data class Score(val gameId: String = "", val username: String = "", var score: Int = 0)
