package be.kdg.boardgame.gamecreation.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class Game(
    @JsonProperty
    var id: String? = "",
    @JsonProperty
    var title: String? = "Scrabble Game",
    @JsonProperty
    var currentlyPlaying: Int? = 0,
    @JsonProperty
    var players: List<User> = listOf(),
    @JsonProperty
    val scores: List<Score> = emptyList(),
    @JsonProperty
    var creator: User = User(),
    @JsonProperty
    var isOver: Boolean = false,
    @JsonProperty
    var hasStarted: Boolean = false,
    @JsonProperty
    var board: String = "",
    @JsonProperty
    var winner: String = ""
)