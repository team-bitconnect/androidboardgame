package be.kdg.boardgame.gamecreation

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SeekBar
import be.kdg.boardgame.BaseNavigationActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.friends.dto.Friend
import be.kdg.boardgame.gamecreation.dto.Game
import be.kdg.boardgame.gamecreation.dto.GameRequest
import be.kdg.boardgame.friends.services.FriendService
import be.kdg.boardgame.gamecreation.services.NewGameService
import be.kdg.boardgame.overview.OverviewActivity
import kotlinx.android.synthetic.main.activity_new_game.*
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.*
import kotlin.coroutines.CoroutineContext


class NewGameActivity : BaseNavigationActivity(), CoroutineScope {
    private lateinit var sharedPref: SharedPreferences
    private lateinit var retrofit: Retrofit
    private lateinit var friendService: FriendService
    private lateinit var newGameService: NewGameService
    private var accessToken: String? = null
    private lateinit var mJob: Job
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_game)

        initConstants()
        initBottomNavBar(0, bottom_navigation, this)

        sharedPref = getSharedPreferences("A", Context.MODE_PRIVATE)
        accessToken = sharedPref.getString(getString(R.string.access_token_key), null)

        val spinnerArray = ArrayList<String>()
        spinnerArray.add("None")
        spinnerArray.add("Random")

        GlobalScope.launch {
            val bearer = accessToken.toString()
            val response = friendService.friends(bearer).execute()
            if (response.isSuccessful) {
                val friends: List<Friend>? = response.body()
                spinnerArray.addAll(friends?.map { it.username }.orEmpty())
            } else {
                throw HttpException(response)
            }
        }

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerArray)
        spinner1.adapter = adapter
        spinner2.adapter = adapter
        spinner3.adapter = adapter
        spinner1.setSelection(1)

        //set first spinner on You
        val adapterCreator = ArrayAdapter(this, android.R.layout.simple_spinner_item, listOf("Me"))
        spinner0.adapter = adapterCreator
        spinner0.isEnabled = false

        progressbar.visibility = ViewGroup.INVISIBLE


        val intent = Intent(this, OverviewActivity::class.java)
        playbtn.setOnClickListener {
            progressbar.visibility = ViewGroup.VISIBLE
            GlobalScope.launch {
                var response: Response<Game>? = null
                GlobalScope.async {
                    val bearer = accessToken.toString()
                    response = newGameService.newGame(
                        bearer,
                        GameRequest(
                            players = mutableListOf(
                                spinner1.selectedItem.toString(),
                                spinner2.selectedItem.toString(),
                                spinner3.selectedItem.toString()
                            ),
                            title = etGameTitle.text.toString(),
                            language = "en-US"
                        )
                    ).execute()
                }.await()

                if (response?.isSuccessful == true) {
                    intent.putExtra("gamecreated", true)
                } else {
                    intent.putExtra("gamecreatederror", response?.message())
                    intent.putExtra("gamecreated", false)
                    //throw HttpException(response)
                }
                startActivity(intent)
            }
        }

    }

    private fun initConstants() {
        sharedPref = getPreferences(Context.MODE_PRIVATE)
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        newGameService = retrofit.create(NewGameService::class.java)
        friendService = retrofit.create(FriendService::class.java)
    }

}
