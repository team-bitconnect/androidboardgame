package be.kdg.boardgame.gamecreation.services

import be.kdg.boardgame.gamecreation.dto.Game
import be.kdg.boardgame.gamecreation.dto.GameRequest
import retrofit2.Call
import retrofit2.http.*

interface NewGameService {

    @POST("api/games")
    fun newGame(
        //DTO?
        @Header("Authorization") authHeader: String,
        @Body requestBody: GameRequest

    ): Call<Game>
}