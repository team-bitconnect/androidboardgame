package be.kdg.boardgame.gamecreation.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class Cell(
    @JsonProperty
    var x: Int,
    @JsonProperty
    var y: Int,
    @JsonProperty
    var multiplier: Int,
    @JsonProperty
    var char: String,
    @JsonProperty
    var lang: String,
    @JsonProperty
    var number: Int
)