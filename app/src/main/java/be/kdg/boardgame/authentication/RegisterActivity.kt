package be.kdg.boardgame.authentication

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import be.kdg.boardgame.HomeActivity
import be.kdg.boardgame.R
import be.kdg.boardgame.authentication.dto.SignUpForm
import be.kdg.boardgame.authentication.services.AuthService
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

class RegisterActivity : AppCompatActivity() {

    private lateinit var retrofit: Retrofit
    private lateinit var authService: AuthService

    private var errorText: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initConstants()
        addEventHandlers()
    }

    private fun initConstants() {
        retrofit = Retrofit.Builder()
            .baseUrl(resources.getString(R.string.backend_url))
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
        authService = retrofit.create(AuthService::class.java)
    }

    private fun addEventHandlers() {
        btnRegister.setOnClickListener {
            emailRegister()
            Log.d("REGISTER", "register request sent, $errorText")

            if (errorText != null) {
                tvError.text = errorText
            } else {
                tvError.text = ""
            }
        }

        btnAlreadyHaveAccount.setOnClickListener {
            startActivity(Intent(applicationContext, LoginActivity::class.java))
        }
    }

    private fun emailRegister() = GlobalScope.launch {
        val email = etEmail.text.toString()
        val username = etUsername.text.toString()
        val password = etPassword.text.toString()
        val passwordConfirm = etPasswordConfirm.text.toString()

        if (password != passwordConfirm) {
            errorText = "Passwords do not match."
        } else {
            val signUpForm = SignUpForm(username, email, password)
            val response = authService.register(signUpForm).execute()

            if (response.code() == 200) {
                errorText = null

                // Go to home screen
                val intent = Intent(this@RegisterActivity, HomeActivity::class.java)
                startActivity(intent)
            } else {
                val json = JSONObject(response.errorBody()?.string())
                errorText = json.getString("message")
            }
        }
        Log.d("REGISTER", "errorText: $errorText")
    }
}