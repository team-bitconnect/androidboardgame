package be.kdg.boardgame.authentication.dto

data class SignUpResponse (
    val message: String = ""
)