package be.kdg.boardgame.authentication.dto

import android.util.Base64

data class SignInHeader (
    val clientId: String,
    val clientSecret: String
) {
    override fun toString(): String {
        return ("Basic " + Base64.encodeToString("$clientId:$clientSecret".toByteArray(), Base64.DEFAULT)).trim()
    }
}