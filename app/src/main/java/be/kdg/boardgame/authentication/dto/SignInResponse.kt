package be.kdg.boardgame.authentication.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class SignInResponse(
    @JsonProperty("access_token")
    val accessToken: String? = null,
    @JsonProperty("token_type")
    val tokenType: String? = null,
    @JsonProperty("refresh_token")
    val refreshToken: String? = null,
    @JsonProperty("expires_in")
    val expiresIn: Long? = null,
    @JsonProperty("scope")
    val scope: String? = null,
    @JsonProperty("jti")
    val jti: String? = null
)