package be.kdg.boardgame

import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import org.junit.Rule
import org.junit.runner.RunWith
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import be.kdg.boardgame.authentication.LoginActivity
import org.hamcrest.CoreMatchers.anything
import org.hamcrest.core.StringContains
import org.junit.Test

@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule<LoginActivity>(
        LoginActivity::class.java
    )

    @Test
    fun testLogin(){
        onView(withId(R.id.etUsername))
            .perform(replaceText("testlogin@gmail.com"))
        closeSoftKeyboard()
        onView(withId(R.id.etPassword))
            .perform(replaceText("testlogin"))
        closeSoftKeyboard()
        onView(withId(R.id.btnLogin))
            .perform(click())
        Thread.sleep(5000)
        onView(withId(R.id.btnLogout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testIntentAlreadyHaveAccount() {
        onView(withId(R.id.btnRegister)).perform(click())
        onView(withId(R.id.btnAlreadyHaveAccount)).perform(click())
        Thread.sleep(2000)
        onView(withId(R.id.btnRegister)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testFriend() {
        onView(withId(R.id.etUsername))
            .perform(replaceText("testlogin@gmail.com"))
            .perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.etPassword))
            .perform(replaceText("testlogin"))
            .perform(ViewActions.closeSoftKeyboard())
        onView(withId(R.id.btnLogin))
            .perform(click())
        Thread.sleep(3000)
        onView(withId(R.id.btnNewGame)).check(matches(isDisplayed()))
            .perform(ViewActions.click())
        Thread.sleep(3000)
        onView(withId(R.id.spinner1))
            .perform(click())
        onData(anything()).atPosition(3)
//            .perform(click())
            .check(matches(ViewMatchers.withText(StringContains.containsString("lotte"))))
    }
}
